package studentwork;

import java.util.ArrayList;

import processing.core.PImage;

public class PlanetSimGame extends mqapp.MQApp{
	
    public String name() {return "Planet Simulation";}
    public String author() {return "Paige Anthony";}
    public String description() {return "Create the perfect planet";}
    
    //GAME DATA STRUCTURE
    GameState state;
    
    long lastTime;
    
    float scale;
    float offsetx, offsety;
    float moveOffsetx, moveOffsety;
    float centreOffsetx, centreOffsety;
    
    ArrayList<Planet> planets;
    Planet selectedPlanet;
    Planet tmpPlanet;
    float tempx, tempy;
    float tempr;
    float temps = 1;
	
    //SETUP ON START
	public void setup() {
        size(displayWidth, displayHeight);
        
        this.state = new TutorialState(this);
        
        this.lastTime = millis();
        
        this.scale = 1;
        this.centreOffsetx = displayWidth/2/this.scale;
        this.centreOffsety = displayHeight/2/this.scale;
        
        this.planets = new ArrayList<Planet>();
    }
	
	//DRAW TO SCREEN
	public void draw() {
        background(255, 0, 0);
        
		//handle timing
		long time = millis();
		float deltaTime = (time - this.lastTime)/1000;
		this.lastTime = time;
		
		 //update game objects & render to screen
		this.state.update(deltaTime);
		this.state.render();
	}

	//WHEN MOUSE PRESSED
	public void mousePressed() {
		this.state.mousePressed();
	}
	
	//WHEN MOUSE RELEASED
	public void mouseReleased() {
		this.state.mouseReleased();
	}
	
	//WHEN KEY PRESSED
	public void keyPressed() {
		this.state.keyPressed();
	}
	
	
	/*
	 * HELPER METHODS
	 */
	
	
	//change game state
	public void setState(GameState state) {
		this.state = state;
	}

	public static final float SCALE_INC = 0.1f;
	public static final float MAX_SCALE = 5;
	public static final float OFFSET_INC = 50f;
	
	//scale world/zoom
	public void zoom(int dir) {
		this.scale += dir * SCALE_INC;
		if (this.scale < SCALE_INC) this.scale = SCALE_INC;
		if (this.scale > MAX_SCALE) this.scale = MAX_SCALE;

		this.centreOffsetx = displayWidth/2/this.scale;
		this.centreOffsety = displayHeight/2/this.scale;
	}
	
	//update the offset of the display
	public void updateOffset() {
		this.offsetx = this.centreOffsetx + this.moveOffsetx;
		this.offsety = this.centreOffsety + this.moveOffsety;
	}
	
	//move view left/right
	public void moveLR(int dir) {
		this.moveOffsetx += dir * OFFSET_INC;
		if (this.moveOffsetx > displayWidth) this.moveOffsetx = displayWidth;
		if (this.moveOffsetx < -displayWidth) this.moveOffsetx = -displayWidth;
	}
	
	//move view up/down
	public void moveUD(int dir) {
		this.moveOffsety += dir * OFFSET_INC;
		if (this.moveOffsety > displayHeight) this.moveOffsety = displayHeight;
		if (this.moveOffsety < -displayHeight) this.moveOffsety = -displayHeight;
	}

	public static final String TUTORIAL_TEXT = ""
			+ "Use wasd to move around\n"
			+ "Use Up Down to zoom in and out\n\n"
			+ "Make your own Solar Systme\n"
			+ "Click to continue!";
	
	//display tutorial screen
	public void tutorialDisplay() {
		textAlign(CENTER);
		textSize(64);
		fill(255);
		stroke(255);
		text(TUTORIAL_TEXT, displayWidth/2 , displayHeight/2 - 64*3);
	}
	
	//display instruction in top left corner
	public void instructionDisplay(String text) {
		textAlign(CORNER);
		textSize(32);
		fill(255);
		stroke(255);
		text(text, 50, 50);
	}
	
	//display space background
	public void spaceBackground() {
		colorMode(RGB);
		background(0);
	}
	
	//display planets
	public void planetsDisplay() {
		for(Planet p : this.planets) {
			p.render();
		}
	}
	
	//display temporary planet
	public void tmpPlanetDisplay() {
		if (this.tmpPlanet != null) this.tmpPlanet.render();
	}
	
	//display planet orbit paths
	public void orbitsOverlay() {
		for(Planet p : this.planets) p.displayOrbit();
		if (this.tmpPlanet != null) this.tmpPlanet.displayOrbit();
	}
	
	public void selectedPlanetOverlay() {
		if (this.selectedPlanet != null) this.selectedPlanet.displaySelectedOverlay();
	}
	
	//return true if a planet has been clicked
	public boolean clickedOnPlanet() {
		for (Planet p : this.planets) {
			if (p.clicked()) {
				if (this.selectedPlanet == p) this.selectedPlanet = null;
				else this.selectedPlanet = p;
				return true;
			}
		}
		return false;
	}
	
	//add temporary planet to list
	public void addPlanet(Planet planet) {
		for (Planet p : this.planets)
			if (p.collides(planet)) {
				this.tmpPlanet = null;
				return;
			}
		
		this.tmpPlanet.initialise();
		this.planets.add(planet);
		this.tmpPlanet = null;
	}
	
	//simulate planet motion
	public void simulatePlanets() {
		for (Planet p : this.planets) {
			p.update();
		}
	}
	
	
	/*
	 * CLASSES
	 */

	
	//PLANET OBJECT
	private class Planet {
		public Planet parent;
		
		public boolean editing = false;

		public float x, y;			//center x y position relative to screen
		public float relx, rely;	//center x y position relative to parent
		public float reld;			//distance from parent
		public float a;				//angle of rotation
		public float r;				//radius
		public static final float MIN_R = 5;
		
		public int hue;				//color
		
		public float s;				//speed of planet orbit
		public float orbitSpeed;	//scaled orbit speed
		public static final float STD_SPEED = 1;
		public static final float SPEED_INC = 1;
		public static final float MIN_SPEED = -10;
		public static final float MAX_SPEED = 20;
		public static final float ORBIT_SPEED_SCALE = 0.1f;
		
		public Planet(Planet parent, float x, float y, float r, float speed, boolean editing) {
			this.parent = parent;
			this.editing = editing;

			setPosition(x, y);
			
			if (r < MIN_R) this.r = MIN_R;
			else this.r = r;
			
			if (speed == 0) this.s = STD_SPEED;
			else this.s = speed;
			
			this.hue = (int)r%360;
			
			initialise();
		}
		
		public void update() {
			if (parent != null) {
				orbitParent();
				updatePosition();
			}
		}
		
		public void render() {
			colorMode(HSB, 360, 100, 100);
			fill(hue, 100, 100);
			stroke(hue, 100, 100);
			strokeWeight(0);
			if (this.editing) {
				noFill();
				strokeWeight(5);
			}
			
			ellipse(getScreenx(), getScreeny(), getScreenr() * 2, getScreenr() * 2);
		}
		
		public void initialise() {
			this.editing = false;
			
			if (this.parent != null) {
				this.relx = this.getScreenx() - this.parent.getScreenx();
				this.rely = this.getScreeny() - this.parent.getScreeny();
				this.reld = dist(this.getScreenx(), this.getScreeny(), this.parent.getScreenx(), this.parent.getScreeny()) / scale;
				this.a = atan(this.rely/this.relx);
				if (this.relx < 0) this.a += PI;
			}

			this.orbitSpeed = radians(this.s) * ORBIT_SPEED_SCALE;
		}
		
		public void setPosition(float mousex, float mousey) {
			this.x = (mousex / scale) - offsetx;
			this.y = (mousey / scale) - offsety;
		}
		
		public void setr(float r) {
			this.r = (r / scale);
			if (r < MIN_R) this.r = MIN_R;
			
			this.hue = (int)r%360;
		}
		
		public void incs(int inc) {
			this.s += inc * SPEED_INC;
			if (this.s < MIN_SPEED) this.s = MIN_SPEED;
			if (this.s == 0) this.s = inc * SPEED_INC;
			if (this.s > MAX_SPEED) this.s = MAX_SPEED;
		}
		
		public float getScreenx() {
			return (this.x + offsetx) * scale;
		}
		
		public float getScreeny() {
			return (this.y + offsety) * scale;
		}
		
		public float getScreenr() {
			return this.r * scale;
		}
		
		public boolean isSmaller(Planet planet) {
			println(this.getScreenr() + " " + planet.getScreenr());
			if (this.getScreenr() < planet.getScreenr()) return true;
			return false;
		}
		
		public void displaySelectedOverlay() {
			strokeWeight(10);
			noFill();
			colorMode(RGB);
			stroke(255, 0, 0);
			ellipse(getScreenx(), getScreeny(), getScreenr() * 2 + 10, getScreenr() * 2 + 10);
		}
		
		public void displayOrbit() {
			if (this.parent != null) {
				float tmpa = this.a;
				while (tmpa < this.a + PI && tmpa > this.a - PI) {
					float x = (this.reld * cos(tmpa)) * scale + this.parent.getScreenx();
					float y = (this.reld * sin(tmpa)) * scale + this.parent.getScreeny();
					
					colorMode(HSB);
					strokeWeight(2);
					stroke(hue, 70, 100);
					point(x, y);
					
					tmpa += radians(this.s) * 4; //only show every 4th
				}
			}
		}

		private void orbitParent() {
			this.a += this.orbitSpeed;
			this.relx = this.reld * cos(this.a);
			this.rely = this.reld * sin(this.a);
		}
		
		private void updatePosition() {
			this.x = this.relx + this.parent.x;
			this.y = this.rely + this.parent.y;
		}
		
		public boolean clicked() {
			if (mouseX > getScreenx() - getScreenr() &&
					mouseX < getScreenx() + getScreenr() &&
					mouseY > getScreeny() - getScreenr() &&
					mouseY < getScreeny() + getScreenr()) {
				return true;
			}
			return false;
		}
		
		public boolean collides(Planet p) {
			if (dist(getScreenx(), getScreeny(), p.getScreenx(), p.getScreeny()) <= getScreenr() + p.getScreenr()) {
				return true;
			}
			return false;
		}
	}
	
	
	//GAME STATES
	private interface GameState {
		public void mousePressed();
		public void mouseReleased();
		public void keyPressed();
		public void keyReleased();
		public void render();
		public void update(float dt);
	}
	
	private class TutorialState implements GameState {
		//STATE DATA STRUC
		private PlanetSimGame game;
		
		//CONSTRUCTOR
		public TutorialState(PlanetSimGame game) {
			println("Tutorial");
			this.game = game;
		}

		//METHODS
		@Override
		public void mousePressed() {
		}

		@Override
		public void mouseReleased() {
			this.game.setState(new CreatingPlanetsState(this.game));
		}

		@Override
		public void keyPressed() {}

		@Override
		public void keyReleased() {}

		@Override
		public void render() {
			this.game.spaceBackground();
			this.game.tutorialDisplay();
		}

		@Override
		public void update(float dt) {}
		
	}
	
	private class CreatingPlanetsState implements GameState {
		//STATE DATA STRUC
		private PlanetSimGame game;

		private String instructions = ""
				+ "If you click a planet you can make other planets which orbit it\n"
				+ "And click it again to deselect it\n"
				+ "Click and hold mouse button to create a planet\n"
				+ "Press Space to start the simulation!";
		
		//CONSTRUCTOR
		public CreatingPlanetsState(PlanetSimGame game) { 
			println("Creating Planets");
			this.game = game;
		}

		//METHODS
		@Override
		public void mousePressed() {
			if (!this.game.clickedOnPlanet()) {
				this.game.tmpPlanet = new Planet(this.game.selectedPlanet, mouseX, mouseY, 0, 0, true);
				this.game.setState(new CustomizingPlanetState(this.game));
			}
		}

		@Override
		public void mouseReleased() {}

		@Override
		public void keyPressed() {
			//control zoom
			if (keyCode == UP) this.game.zoom(1);
			if (keyCode == DOWN) this.game.zoom(-1);
			
			//control movement
			if (key == 'a') this.game.moveLR(1);
			if (key == 'd') this.game.moveLR(-1);
			if (key == 'w') this.game.moveUD(1);
			if (key == 's') this.game.moveUD(-1);
			
			//start simulation
			if (key == ' ') this.game.setState(new SimulatingPlanetsState(this.game));
		}

		@Override
		public void keyReleased() {}

		@Override
		public void render() {
			this.game.spaceBackground();
			this.game.instructionDisplay(this.instructions);
			this.game.planetsDisplay();
			this.game.orbitsOverlay();
			this.game.selectedPlanetOverlay();
		}

		@Override
		public void update(float dt) {
			this.game.updateOffset();
		}
		
	}
	
	private class CustomizingPlanetState implements GameState {
		//STATE DATA STRUC
		private PlanetSimGame game;
		
		private String instructions = ""
				+ "Drag to resize\n"
				+ "If planet has parent, 'f' to increase speed anticlockwise\n"
				+ "If planet has parent, 'g' to increase speed clockwise\n"
				+ "Release mouse button to set the planet\n";
		
		//CONSTRUCTOR
		public CustomizingPlanetState(PlanetSimGame game) {
			println("Customizing Planet");
			this.game = game;
		}

		//METHODS
		@Override
		public void mousePressed() {}

		@Override
		public void mouseReleased() {
			this.game.addPlanet(this.game.tmpPlanet);
			this.game.setState(new CreatingPlanetsState(this.game));
		}

		@Override
		public void keyPressed() {
			//control new planet speed
			if (key == 'f') this.game.tmpPlanet.incs(-1);
			if (key == 'g') this.game.tmpPlanet.incs(1);
		}

		@Override
		public void keyReleased() {}

		@Override
		public void render() {
			this.game.spaceBackground();
			this.game.instructionDisplay(this.instructions);
			this.game.planetsDisplay();
			this.game.orbitsOverlay();
			this.game.selectedPlanetOverlay();
			this.game.tmpPlanetDisplay();
		}

		@Override
		public void update(float dt) {
			this.game.tmpPlanet.setr(dist(this.game.tmpPlanet.getScreenx(), this.game.tmpPlanet.getScreeny(), mouseX, mouseY));
		}
		
	}
	
	private class SimulatingPlanetsState implements GameState {
		//STATE DATA STRUC
		private PlanetSimGame game;

		private String instructions = ""
				+ "Press Space to return to editing";
		
		//CONSTRUCTOR
		public SimulatingPlanetsState(PlanetSimGame game) {
			println("Simulation");
			this.game = game;
		}

		//METHODS
		@Override
		public void mousePressed() {}

		@Override
		public void mouseReleased() {}

		@Override
		public void keyPressed() {
			//control zoom
			if (keyCode == UP) this.game.zoom(1);
			if (keyCode == DOWN) this.game.zoom(-1);
			
			//control movement
			if (key == 'a') this.game.moveLR(1);
			if (key == 'd') this.game.moveLR(-1);
			if (key == 'w') this.game.moveUD(1);
			if (key == 's') this.game.moveUD(-1);
			
			//end simulation
			if (key == ' ') this.game.setState(new CreatingPlanetsState(this.game));
		}

		@Override
		public void keyReleased() {}

		@Override
		public void render() {
			this.game.spaceBackground();
			this.game.instructionDisplay(this.instructions);
			this.game.planetsDisplay();
		}

		@Override
		public void update(float dt) {
			this.game.updateOffset();
			this.game.simulatePlanets();
		}
		
	}
}

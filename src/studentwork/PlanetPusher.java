// by Elise McCabe, converted to macquarie mini my Matt Roberts

package studentwork;

import processing.core.PImage;

public class PlanetPusher extends mqapp.MQApp {

    public String name(){return "Planet Pusher";}
    public String author(){return "Benjamin van de Vorstenbosch";}
    public String description(){return "Push the earth into the future!";}

    //Image sources
    //Planet: https://dribbble.com/shots/1697702-Earth-Gif

    //Images
    PImage planetImageLarge;
    PImage planetImageSmall;
    PImage planetImage;

    //Currency
    int OPS = 0; //Oxygen per second
    int multiplier = 1;
    int oxygenCount = 0;

    //Ratios
    int waterGrassRatio = 3;
    int grassTreeRatio = 10;
    int treeAppleRatio = 8;
    int applePersonRatio = 2;
    int personHouseRatio = 4;
    int houseCityRatio = 1000;


    //Costs

    //Risk


    //Misc
    boolean canPush = true;
    boolean wasPressed = false;
    int pressCounter = 0;
    int pressCounterMax = 5;


    public void setup() {
        size(displayWidth, displayHeight);

        //Images
        imageMode(CENTER);
        planetImageLarge = loadImage("../data/planet_pusher/images/earth_large.png");
        planetImageSmall = loadImage("../data/planet_pusher/images/earth_small.png");
        planetImage = planetImageLarge;
    }

    public void draw() {
        //Defaults
        background(color(237, 239, 240));
        fill(0);
        stroke(0);
        strokeWeight(1);
        shapeMode(CORNER);
        textSize(32);

        //Render objects
        imageMode(CENTER);
        image(planetImage, width/2, height/2);
        text(oxygenCount, 100, 100);

        //Run functions
        planetAnimation();
    }

    public void mouseClicked() {
        pushPlanet();
    }

    public void keyPressed() {
        if(key == ' ' && canPush) {
            pushPlanet();
            canPush = false;
        }
    }

    public void keyReleased() {
        if(key == ' ') {
            canPush = true;
        }
    }

    void pushPlanet() {
        planetImage = planetImageSmall;
        wasPressed = true;

        oxygenCount += multiplier;
    }

    void planetAnimation() {
        //Button press animation
        if(wasPressed) {
            pressCounter++;
            if(pressCounter > pressCounterMax) {
                planetImage = planetImageLarge;

                wasPressed = false;
                pressCounter = 0;
            }
        }
    }
}
